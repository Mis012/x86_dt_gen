#!/usr/bin/env python3

import sys

from parse import *

import multiprocessing

import subprocess

first_time = 1

ioapics = []
pci_host_controllers = []
ranges_str = ""

# can this be trusted? I mean, probably...
NUM_CPUS = multiprocessing.cpu_count()

proc_iomem = open("/proc/iomem")

print(
    """/dts-v1/;

#define PCI_BDF(b, d, f) ( (b << 16) | (d << 11) | (f << 8) )

/ {
	model = "fixme:vendor,device";
	compatible = "fixme:vendor,device";

	#address-cells = <1>;
	#size-cells = <1>;

	chosen {
		#address-cells = <1>;
		#size-cells = <1>;

		/*
		 * uncomment the following line if the firmware pre-allocates the BARs and Linux
		 * is unable to re-allocate them for some reason
		 *
		 * Linux supports taking "suggestions" for allocating the BARs from ACPI, it should
		 * probably also support taking them from DT (the ePAPR spec for PCI describes
		 * bindings which could be used for this purpose)
		 */
//		linux,pci-probe-only = <1>;
	};

	cpus {
		#address-cells = <1>;
		#size-cells = <0>;
"""
)

for i in range(0, NUM_CPUS):
	print(
	    """
		cpu@%d {
			compatible = "intel,ce4100";
			reg = <0x%02x>;

			device_type = "cpu";
			lapic = <&lapic0>;
		};""" % (i, i)
	)

print("\t};\n")

print(
    """	soc {
		compatible = "simple-bus";

		#address-cells = <1>;
		#size-cells = <1>;
		ranges;"""
)

for line in proc_iomem:
	if "PCI MMCONFIG" in line:
		pci_host_controllers.append(parse("{}-{} : PCI MMCONFIG {} [bus {}-{}]", line.strip()))
	if "Local APIC" in line:
		(lapic_start, lapic_end) = parse("{}-{} : Local APIC", line.strip())
		lapic_start = int(lapic_start, 16)
		lapic_end = int(lapic_end, 16)
	if "HPET" in line:
		#		can there be more than one HPET? should we care?
		(hpet_start, hpet_end) = parse("{}-{} : HPET 0", line.strip())
		hpet_start = int(hpet_start, 16)
		hpet_end = int(hpet_end, 16)
	if "IOAPIC" in line:
		ioapics.append(parse("{}-{} : IOAPIC {}", line.strip()))
	if "PCI Bus" in line:
		(start, end, bus) = parse("{}-{} : PCI Bus {}", line.strip())

		start = int(start, 16)
		size = int(end, 16) - start + 1

		if bus == "0000:00":
			ranges_str += (
			    "//" if first_time else "//\t\t\t\t  "
			) + "0x03000000 0 " + hex(start) + " /*|*/ " + hex(start) + " /*|*/ 0 " + hex(
			    size
			) + "\t// " + bus + ":*.* (this is the root bus, so you should manually review if this entry is not bogus)\n"
		else:
			ranges_str += ("" if first_time else "\t\t\t\t  ") + "0x03000000 0 " + hex(
			    start
			) + " /*|*/ " + hex(start) + " /*|*/ 0 " + hex(size) + "\t// " + bus + ":*.*\n"

		first_time = 0

print()

for (ioapic_start, ioapic_end, ioapic_id) in ioapics:
	ioapic_start = int(ioapic_start, 16)
	ioapic_end = int(ioapic_end, 16)
	ioapic_id = int(ioapic_id)

	print(
	    """
		/* ioapicX - may need to edit; should be 1 and 2 (for two ioapics) */
		ioapic%d: interrupt-controller@%x {
			compatible = "intel,ce4100-ioapic";
			reg = <0x%x 0x%x>;

			interrupt-controller;
			#interrupt-cells = <2>;
		};
	""" % (ioapic_id + 1, ioapic_start, ioapic_start, ioapic_end - ioapic_start + 1)
	)

print(
    """
		timer@%x {
			compatible = "intel,ce4100-hpet";
			reg = <0x%x 0x%x>;
			// add the ineterrupt maybe?
		};
""" % (hpet_start, hpet_start, hpet_end - hpet_start + 1)
)

print(
    """
		lapic0: interrupt-controller@%x {
			compatible = "intel,ce4100-lapic";
			reg = <0x%x 0x%x>;
		};
""" % (lapic_start, lapic_start, lapic_end - lapic_start + 1)
)

for (pci_start, pci_end, pci_domain, pci_bus_first, pci_bus_last) in pci_host_controllers:
	print(
	    """
		/*
		 * NOTE: since x86 is traditionally 🤡, PCI initializazion is most likely done by the firmware
		 * if it's not, you may need to write a PCI host controller driver instead of just using "pci-host-ecam-generic"
		 */

		pci: pci@%x {
			#address-cells = <3>; // phys.hi should contain the device's BDF as 0b00000000 bbbbbbbb dddddfff 00000000. The other cells should be zero.
			#size-cells = <2>; // 64bit
			compatible = "pci-host-ecam-generic", "pci";
			device_type = "pci";

			msi-parent = <&lapic0>;

			reg = <0x%x 0x%x>; // the ECAM address""" %
	    (int(pci_start, 16), int(pci_start, 16), int(pci_end, 16) - int(pci_start, 16) + 1)
	)

	print(
	    """\t\t\t/*
			 * NOTE: if you have more than one pci host controller, you will need to manually remove
			 * the mappings meant for the other domains (and configure the iospace mapping appropriately)
			 */"""
	)

	print(
	    "\t\t\tranges = <" + ranges_str
	    + "\n\t\t\t\t  0x01000000 0          0 /*|*/          0 /*|*/ 0 0x10000>;\n"
	)

	print(
	    """			bus-range = <0x%x 0x%x>;
			linux,pci-domain = <%d>;

			#interrupt-cells = <1>;
			interrupt-map-mask = <0xf800 0 0 7>;
""" % (int(pci_bus_first, 16), int(pci_bus_last, 16), int(pci_domain))
	)

	print(
	    """\t\t\t/*
			 * NOTE: if you have more than one pci host controller, this will definitely break (see _prt.py)
			 */"""
	)

	p = subprocess.Popen(
	    "acpiexec -b \"evaluate _PIC 1; all _PRT\" /sys/firmware/acpi/tables/DSDT | ./_prt.py",
	    shell=True,
	    stdout=subprocess.PIPE,
	    stderr=subprocess.PIPE
	)
	print(p.communicate()[0].decode("utf-8"))

	print("""		};
	};
};""")
