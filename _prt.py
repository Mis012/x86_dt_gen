#!/usr/bin/env python3

import sys

from parse import *

# `dmesg | grep APIC`
FIRST_APIC2_GSI = 24

# but are the buses ordered sequentially? can't seem to find any info on that
bus = 0

# and fwiw, if we have multiple pcie host controllers, that will make it even more fun...

print("\t\t\tinterrupt-map = <", end='')
PREFIX = "\n\t\t\t\t\t "

first_time = 1

for line in sys.stdin:
	if line.startswith("  [Package]"):
		#		print("/* group start */\n");
		if not first_time:
			print("\n", end='')
		for line in sys.stdin:
			if (line == "    [Package] Contains 4 Elements:\n"):
				df = int(parse("      [Integer] = {}", sys.stdin.readline())[0], 16)
				pin = int(parse("      [Integer] = {}", sys.stdin.readline())[0], 16)
				dunno = int(parse("      [Integer] = {}", sys.stdin.readline())[0], 16)
				irq = int(parse("      [Integer] = {}", sys.stdin.readline())[0], 16)

				# -
				device = df >> 16

				bdf = (bus << 16) | (device << 11)
				# -
				if irq >= FIRST_APIC2_GSI:
					irq_str = "&ioapic2 " + str(irq - FIRST_APIC2_GSI)
				else:
					irq_str = "&ioapic1 " + str(irq)

				print(
				    ("" if first_time else PREFIX) + "PCI_BDF(0x%x, 0x%x, 0x%x)" % (bus, device, 0) + " 0 0 " + str(pin + 1) + " "
				    + irq_str + " 1",
				    end=''
				)
				first_time = 0
			elif line == "\n":
				bus += 1
				break
			else:
				print("unexpected input >" + line + "<, bailing")
				sys.exit(1)

print(">;")

#regarding mr. 'dunno'

#from PCI Interrupts for x86 Machines under FreeBSD by John H. Baldwin

#The third and fourth members define the
#destination of the interrupt. If the third value
#is either zero or an empty string, then the
#fourth value is a GSI and the PCI interrupt
#is hard-wired to that GSI. This type of map-
#ping is just like the MP Table I/O interrupt
#entries which map PCI interrupts to specific
#I/O APIC input pins. If the third value is not
#empty, then it is the name of a PCI interrupt
#link device in the ACPI namespace, and the
#fourth value is a resource index. The resource
#index indicates which of the resources of the
#PCI interrupt link device the interrupt is con-
#nected to. In practice, though, some BIOSes
#bogusly include a name of a link device even
#in entries that are routed via a hardwired
#GSI, and no systems to date use a resource
#index other than zero. Thus, as a workaround
#for the busted systems, FreeBSD ignores the
#third value and assumes the fourth value is a
#hardwired GSI if the fourth value is not zero.
